﻿using UnityEngine;
using System.Collections;

public class ScoreManagerScript : MonoBehaviour {

    public static int Score { get; set; }

    IEnumerator fadeInAndOut(GameObject objectToFade, bool fadeIn, float duration)
    {
        float counter = 0f;

        //Set Values depending on if fadeIn or fadeOut
        float a, b;
        if (fadeIn)
        {
            a = 0;
            b = 1;
        }
        else
        {
            a = 1;
            b = 0;
        }

        int mode = 0;
        Color currentColor = Color.clear;

        SpriteRenderer tempSPRenderer = objectToFade.GetComponent<SpriteRenderer>();
        MeshRenderer tempRenderer = objectToFade.GetComponent<MeshRenderer>();

        //Check if this is a Sprite
        if (tempSPRenderer != null)
        {
            currentColor = tempSPRenderer.color;
            mode = 0;
        }

        //Check if 3D Object
        else if (tempRenderer != null)
        {
            currentColor = tempRenderer.material.color;
            mode = 4;

            //ENABLE FADE Mode on the material if not done already
            tempRenderer.material.SetFloat("_Mode", 2);
            tempRenderer.material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            tempRenderer.material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            tempRenderer.material.SetInt("_ZWrite", 0);
            tempRenderer.material.DisableKeyword("_ALPHATEST_ON");
            tempRenderer.material.EnableKeyword("_ALPHABLEND_ON");
            tempRenderer.material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            tempRenderer.material.renderQueue = 3000;
        }
        else
        {
            yield break;
        }

        while (counter < duration)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);

            switch (mode)
            {
                case 0:
                    tempSPRenderer.color = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);
                    break;
                case 1:
                    tempRenderer.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);
                    break;
            }
            yield return null;
        }
    }

    void Start () {
        (Tens.gameObject as GameObject).SetActive(false);
        (Hundreds.gameObject as GameObject).SetActive(false);
    }
	
	
	void Update () {
        if (GameStateManager.GameState == GameState.Intro)
        {
            Score = 0;
            (Background.gameObject as GameObject).SetActive(true);
            (Background1.gameObject as GameObject).SetActive(false);
            (Background2.gameObject as GameObject).SetActive(false);
            (Background3.gameObject as GameObject).SetActive(false);
            (Background4.gameObject as GameObject).SetActive(false);
            (Background5.gameObject as GameObject).SetActive(false);
            (Background6.gameObject as GameObject).SetActive(false);
        }

        if (previousScore != Score) 
        { 
            if(Score < 10)
            {

                Units.sprite = numberSprites[Score];
            }
            else if(Score >= 10 && Score < 100)
            {
                (Tens.gameObject as GameObject).SetActive(true);
                Tens.sprite = numberSprites[Score / 10];
                Units.sprite = numberSprites[Score % 10];
            }
            else if(Score >= 100)
            {
                (Hundreds.gameObject as GameObject).SetActive(true);
                Hundreds.sprite = numberSprites[Score / 100];
                int rest = Score % 100;
                Tens.sprite = numberSprites[rest / 10];
                Units.sprite = numberSprites[rest % 10];
            }

            //BackgroundChange
            if(Score < 30)
            {
                (Background.gameObject as GameObject).SetActive(true);
                (Background1.gameObject as GameObject).SetActive(false);
                (Background2.gameObject as GameObject).SetActive(false);
                (Background3.gameObject as GameObject).SetActive(false);
                (Background4.gameObject as GameObject).SetActive(false);
                (Background5.gameObject as GameObject).SetActive(false);
                (Background6.gameObject as GameObject).SetActive(false);
            } 
            else if (Score >= 30 && Score <= 50 )
            {

                StartCoroutine(fadeInAndOut(Background.gameObject, false, 5f));
                (Background1.gameObject as GameObject).SetActive(true);
                StartCoroutine(fadeInAndOut(Background1.gameObject, true, 3f));
                (Background2.gameObject as GameObject).SetActive(false);
                (Background3.gameObject as GameObject).SetActive(false);
                (Background4.gameObject as GameObject).SetActive(false);
                (Background5.gameObject as GameObject).SetActive(false);
                (Background6.gameObject as GameObject).SetActive(false);
            }
            else if (Score >= 51 && Score <= 70)
            {
                StartCoroutine(fadeInAndOut(Background1.gameObject, false, 5f));
                (Background2.gameObject as GameObject).SetActive(true);
                StartCoroutine(fadeInAndOut(Background2.gameObject, true, 3f));
                (Background3.gameObject as GameObject).SetActive(false);
                (Background4.gameObject as GameObject).SetActive(false);
                (Background5.gameObject as GameObject).SetActive(false);
                (Background6.gameObject as GameObject).SetActive(false);
            }
            else if (Score >= 71 && Score <= 100)
            {
                StartCoroutine(fadeInAndOut(Background2.gameObject, false, 5f));
                (Background3.gameObject as GameObject).SetActive(true);
                StartCoroutine(fadeInAndOut(Background3.gameObject, true, 3f));
                (Background4.gameObject as GameObject).SetActive(false);
                (Background5.gameObject as GameObject).SetActive(false);
                (Background6.gameObject as GameObject).SetActive(false);
            }
            else if (Score >= 101 && Score <= 140)
            {
                StartCoroutine(fadeInAndOut(Background3.gameObject, false, 5f));
                (Background4.gameObject as GameObject).SetActive(true);
                StartCoroutine(fadeInAndOut(Background4.gameObject, true, 3f));
                (Background5.gameObject as GameObject).SetActive(false);
                (Background6.gameObject as GameObject).SetActive(false);
            }
            else if (Score >= 141 && Score <= 170)
            {
                StartCoroutine(fadeInAndOut(Background4.gameObject, false, 5f));
                (Background5.gameObject as GameObject).SetActive(true);
                StartCoroutine(fadeInAndOut(Background5.gameObject, true, 3f));
                (Background6.gameObject as GameObject).SetActive(false);
            }
            else if (Score >= 200)
            {
                StartCoroutine(fadeInAndOut(Background5.gameObject, false, 5f));
                (Background6.gameObject as GameObject).SetActive(true);
                StartCoroutine(fadeInAndOut(Background6.gameObject, true, 3f));
            }
        }

	}


    int previousScore = -1;
    public Sprite[] numberSprites;
    public SpriteRenderer Units, Tens, Hundreds, Background, Background1, Background2, Background3, Background4, Background5, Background6;
}
