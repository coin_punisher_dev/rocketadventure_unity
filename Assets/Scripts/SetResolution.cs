﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SetResolution : MonoBehaviour {

    public GameObject soundmaingame_object;
    public AudioSource soundmaingame_audio_source;

    public IEnumerator checkInternetConnection(System.Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }
	
	void OnApplicationQuit()
    {
        Login Udata = new Login();
		Debug.Log(PlayerPrefs.GetString("bossgame_nickname"));
		StartCoroutine(Udata.postQuit(PlayerPrefs.GetString("bossgame_nickname")));
		StartCoroutine(Udata.postPlayTimePlayingGame(PlayerPrefs.GetString("bossgame_iduser"), "16", PlayerPrefs.GetString("FirstPlay"), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
		Debug.Log(PlayerPrefs.GetString("FirstPlay") + " " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
		Application.Quit();
    }

    void Start()
    {
        // Switch to 640 x 480 full-screen
        PlayerPrefs.SetString("FirstPlay", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Screen.SetResolution(1080, 1920, true);

        if (!soundmaingame_audio_source.isPlaying)
        {
            soundmaingame_audio_source = soundmaingame_object.GetComponent<AudioSource>();
            soundmaingame_audio_source.GetComponent<AudioSource>().enabled = true;
            soundmaingame_audio_source.Play();
        }

        StartCoroutine(checkInternetConnection((isConnected) => {
            if (!isConnected) {
                SceneManager.LoadScene(2);
            }
        }));
    }
}
