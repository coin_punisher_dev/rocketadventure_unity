﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
//using GoogleMobileAds.Api;
using UnityEngine.Advertisements;
using System;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour
{
	public string gameId = "3437556";
    public string myPlacementId = "rewardedVideo";
    public bool testMode = false;
	
    public string base_url = "https://bossgame.co.id/webServices/";
    public Collider2D SinglePlayerPlay;
    public GameObject FirstPage, SecondPage;
    public int statusPlay;
    public RawImage avatar2;
    public string bossgame_email;
    public string bossgame_username;
    public InputField bossgame_nickname;
    public string bossgame_iduser;
    public string bossgame_idgame;
    public string bossgame_avatar;
    public int bossgame_idvalue;
    public string bossgame_valmessage;

    public Text sisa_main, sisa_main_2_message, sisa_main_2, sisa_main_3, error_message_login, UsernameBossgame2;


    
    public string time_end_str;
    public string TimePlaying2;
    public Collider2D LoginBPlay;
	
	public InputField IFMemberID;
	
	public GameObject SendMemberIDButton;
	
	//public bool klikPlay;

    
	public IEnumerator postGetProfile(string nickname)
    {
        WWWForm form = new WWWForm();
        form.AddField("nickname", nickname);
        form.AddField("gameID", "16");

        UnityWebRequest www = UnityWebRequest.Post(base_url + "cekNickName.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
			Debug.Log(nickname);
            Debug.Log("Success Send API");
            string a = www.downloadHandler.text;
			Debug.Log(a);
            var jsonObject = JSON.Parse(a);
			if(jsonObject["value"]==2){
			bossgame_valmessage = jsonObject["message"];
			PlayerPrefs.SetInt("klikPlay",1);
			GameStateManager.GameState = GameState.Intro;
			error_message_login.text = bossgame_valmessage;
			PlayerPrefs.SetInt("klikPlay",1);
			}else{
            bossgame_iduser = jsonObject["id"];
            PlayerPrefs.SetString("bossgame_iduser", jsonObject["id"]);
            bossgame_email = jsonObject["email"];
            bossgame_username = jsonObject["username"];
            PlayerPrefs.SetString("bossgame_username", jsonObject["username"]);
            bossgame_avatar = jsonObject["photo"];
            PlayerPrefs.SetString("bossgame_avatar", jsonObject["photo"]);
            bossgame_idvalue = jsonObject["value"];
            bossgame_valmessage = jsonObject["message"];
            PlayerPrefs.SetString("bossgame_heart", jsonObject["heart"]);
			
            if (bossgame_idvalue == 1)
            {
                this.statusPlay = 1;
                FirstPage.SetActive(true);
				SecondPage.SetActive(false);
				
				PlayerPrefs.SetInt("klikPlay",0);
				//int heartMinus = int.Parse(PlayerPrefs.GetString("bossgame_heart")) - 1;
				//PlayerPrefs.SetString("bossgame_heart", heartMinus.ToString());
            }
            else
            {
                error_message_login.text = bossgame_valmessage;
				PlayerPrefs.SetInt("klikPlay",1);
				
            }
            //Debug.Log(bossgame_iduser + bossgame_idgame);
            //StartCoroutine(LoadImgAvatar(base_url + "profile/" + bossgame_avatar));
			}
		}
	}
    void Start()
    {
		Debug.Log(testMode);
        Debug.Log(PlayerPrefs.GetString("statusAds"));
		PlayerPrefs.SetInt("klikPlay",1);
		//this.klikPlay=true;
        Advertisement.Initialize ("3437556", false);
    }
    void OnMouseDown()
    {
		if(bossgame_nickname.text.ToString() == ""){
			bossgame_username = "-";
		}else{
			bossgame_username = IFMemberID.text.ToString();
		}
		Debug.Log("1");
		//Debug.Log(klikPlay);
		Debug.Log(IFMemberID.text.ToString());
		if(PlayerPrefs.GetInt("klikPlay") == 1){
			Debug.Log("2");
			//Debug.Log(klikPlay);
			Debug.Log(IFMemberID.text.ToString());
			PlayerPrefs.SetString("bossgame_nickname", IFMemberID.text.ToString());
			StartCoroutine(postGetProfile(IFMemberID.text.ToString()));
		}
		//this.klikPlay=false;
        
    }
	
	public IEnumerator postQuit(string iu)
    {
		WWWForm form = new WWWForm();
        form.AddField("nickname", iu);

        UnityWebRequest www = UnityWebRequest.Post(base_url + "quitGame.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API Quit");
			string a = www.downloadHandler.text;
			Debug.Log(a);
            Debug.Log(iu);
        }
    }

    public IEnumerator postPlayTimePlayingGame(string iu, string ig, string st, string et)
    {
        Login Udata = new Login();
        WWWForm form = new WWWForm();
        form.AddField("idUsers", iu);
        form.AddField("idGame", ig);
        form.AddField("startTime", st);
        form.AddField("endTime", et);

        UnityWebRequest www = UnityWebRequest.Post(Udata.base_url + "playGameEndpoint.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Success Send API");
        }
    }
}